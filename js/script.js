'use strict'

const input = document.querySelector('.autocomplete-input')
const list = document.querySelector('ul.list')


const pressKeyDown = () => {
    let activeContains = false
    // Jeder List-Element prüfen wir. Wenn einer davon active class hat, dann weisen wir activeContains true zu.
    for(let li of list.querySelectorAll('li')) { 
        if(li.classList.contains('active')) {
            activeContains = true
            break
        }
    }
    // Wenn die Variable noch false und erster Kind-Element vorhanden ist, dann machen wir erster Kind-Element active    
    if(activeContains === false && list.querySelector('li:first-child')) {       
        list.querySelector('li:first-child').classList.add('active')
        return
    } 
    // Wenn die Variable true ist, machen wir naexter Geschwister-Element active    
    for(let li of list.querySelectorAll('li')) {            
        if(li.classList.contains('active')) {
            li.classList.remove('active')      
            // li.nextSibling.className = 'active'
            // Bir sonraki element var mı kontrol edilir, varsa active class ı atanır               
            if(li.nextSibling) li.nextSibling.classList.add('active')                
            break;
        }
    } 
}

const pressKeyUp = () => {
    for(let li of list.querySelectorAll('li')) {            
        if(li.classList.contains('active')) {
            li.classList.remove('active')             
            if(li.previousSibling) li.previousSibling.classList.add('active')                
            break;
        }
    }     
}

const emptyList = () => {
    list.classList.remove('d-block')
    list.classList.add('d-none')
    list.innerHTML = '' 
}

const pressEnter = () => {
    for(let li of list.querySelectorAll('li')) { 
        if(li.classList.contains('active')) {
            input.value = li.textContent
             emptyList()         
        }
    }      
}

const fetchCities = async () => {
    const response = await fetch('de.json')
    return await response.json()
}

const capitalFirstLetter = string => {
    return string.charAt(0).toUpperCase() + string.substring(1)
}

const showList = () => {
    list.classList.remove('d-none')
    list.classList.add('d-block')     
}

const clickListElement = event => {   
    input.value = event.target.textContent
    emptyList()
}

const createList = categorieName => {       
    let listElement = document.createElement('li');
    listElement.textContent = categorieName    
    list.appendChild(listElement)

    list.querySelectorAll('li').forEach(liElement => {
        liElement.addEventListener('click', clickListElement)
    })    
    showList()
}

const typeWords = () => {
    fetchCities()
        .then(response => {                   
            const valueWithUpperCase = capitalFirstLetter(input.value)
            // Die Liste wird geleert
            emptyList()
            response.cities.forEach(city => {               
                if(valueWithUpperCase !== '' && city.name.includes(valueWithUpperCase)) {
                    // Die Liste wird wieder mit den neuen Daten erstellt
                    createList(city.name)
                }
            });
        });
}

const autocomplete = (event) => {
    switch(event.key) {
        case 'ArrowDown'   : pressKeyDown(); break;
        case 'ArrowUp'     : pressKeyUp();   break;
        case 'Enter'       : pressEnter();   break;
        default            : typeWords();
    }
}

input.addEventListener('keyup', autocomplete)